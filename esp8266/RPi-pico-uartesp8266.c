/**
 * Copyright (c) 2008-2021 深圳百问网科技有限公司<https://www.100ask.net/>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "hardware/gpio.h"
#include "hardware/uart.h"

#define UART_ID uart1
#define BAUD_RATE 115200

#define UART_TX_PIN 4
#define UART_RX_PIN 5

#define RECV_BUFFER 256
char recv_buf[RECV_BUFFER];
int buff_count = 0;

void core1_entry() 
{
    memset(recv_buf,'0',RECV_BUFFER);
    while (1) {
        //printf("%c", uart_getc(UART_ID));
        recv_buf[buff_count++] = uart_getc(UART_ID);         
        if(buff_count >= RECV_BUFFER )   buff_count = 0;
    }
}

int main() {
    stdio_init_all();

    // Set up our UART with the required speed.
    uart_init(UART_ID, BAUD_RATE);

    // Set the TX and RX pins by using the function select on the GPIO
    // Set datasheet for more information on function select
    gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
    gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);

    multicore_launch_core1(core1_entry);

    printf("Setting up network on ESP8266...\n");
    printf("  - Setting CWMODE to 1 station mode...\n");
    uart_puts(UART_ID, "AT+CWMODE=1\r\n");
    sleep_ms(2000);
    printf("  - Joining Wifi ...'\n");
    uart_puts(UART_ID, "AT+CWJAP=\"Programmers\",\"100asktech\"\r\n");
    sleep_ms(5000);
    printf("done!\n\n");

    printf("Starting Webserver port on ESP8266...\n");
    sleep_ms(1000);
    printf("  - Setting CIPMUX for multiple connections...\n");
    uart_puts(UART_ID, "AT+CIPMUX=1\r\n");
    sleep_ms(2000);
    printf("  - Starting CIPSERVER on port 80...\n");
    uart_puts(UART_ID, "AT+CIPSERVER=1,80\r\n");
    printf("done!\n\n");

    sleep_ms(1000);
    // 查询本地 IP 地址
    uart_puts(UART_ID, "AT+CIFSR\r\n");
    sleep_ms(1000);
    printf("%s\n", recv_buf);  
    printf("Waiting For connection...\n");
    while (true) {
        if (strstr(recv_buf, "+IPD"))
        {    
            printf("! Incoming connection - sending webpage");
            uart_puts(UART_ID, "AT+CIPSEND=0,108\r\n");  // Send a HTTP response then a webpage as bytes the 108 is the amount of bytes you are sending, change this if you change the data sent below
            sleep_ms(1000);
            uart_puts(UART_ID, "HTTP/1.1 200 OK\r\n");
            uart_puts(UART_ID, "Content-Type: text/html\r\n");
            uart_puts(UART_ID, "Connection: close\r\n");
            uart_puts(UART_ID, "\r\n");
            uart_puts(UART_ID, "<!DOCTYPE HTML>\r\n");
            uart_puts(UART_ID, "<html>\r\n");
            uart_puts(UART_ID, "It Works!\r\n");
            uart_puts(UART_ID, "</html>\r\n");
            sleep_ms(1000);
            uart_puts(UART_ID, "AT+CIPCLOSE=0\r\n");    // close the connection when done.
            sleep_ms(4000);
            printf("\n\nWaiting For connection...\n");
            sleep_ms(250);
            memset(recv_buf,'0',RECV_BUFFER);
            buff_count = 0;
        }
    }
}
