cmake_minimum_required(VERSION 3.16.0)

project(FatFS_SPI_example C CXX ASM)

set(CMAKE_C_STANDARD 11)
set(CMAKE_CXX_STANDARD 17)

add_executable(FatFS_SPI_example
        FatFS_SPI_example.c
        )


add_subdirectory(./lib/FatFs_SPI_lib build)


target_compile_options(FatFS_SPI_example PUBLIC -Wall -Wextra -Wno-unused-function -Wno-unused-parameter)
IF (NOT DEFINED N_SD_CARDS)
    SET(N_SD_CARDS 1)
ENDIF()
target_compile_definitions(FatFS_SPI_example PUBLIC DEBUG N_SD_CARDS=${N_SD_CARDS})

pico_set_program_name(FatFS_SPI_example "100ASK FatFS_SPI_example")
pico_set_program_version(FatFS_SPI_example "0.1")

pico_enable_stdio_uart(FatFS_SPI_example 1)
pico_enable_stdio_usb(FatFS_SPI_example 0)

# Pull in our (to be renamed) simple get you started dependencies
target_link_libraries(FatFS_SPI_example pico_stdlib hardware_spi hardware_clocks hardware_adc FatFs_SPI_lib)


# create map/bin/hex file etc.
pico_add_extra_outputs(FatFS_SPI_example)

# add url via pico_set_program_url
example_auto_set_url(FatFS_SPI_example)
